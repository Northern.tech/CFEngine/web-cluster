#!/bin/bash -e
#
# $1 -- user@host to login to the machine to setup AWX on
# $2 -- path to the private key to install on AWX (so that it can access the hosts)
#

if [ $# -lt 2 ]; then
  echo "usage: $0 [user@]AWX_HOST_IP /path/to/private/ssh_key"
  exit 1
fi

trap 'echo FAILURE' ERR

function step() {
  printf '\e[0;32m'
  echo "$@"
  printf '\e[0;0m'
}

awx_host_login="$1"
key_path="$2"

# Split "user@host" into "user" and "host" or use the whole string as host if no
# "user@" prefix.
user="${awx_host_login%%@*}"
if [ "$user" = "$awx_host_login" ]; then
  # no '@*' suffix removed -> only host given, use the current user
  user="$USER"
fi

SSH_ARGS="-q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"
LOCAL_PORT=9980
if fuser ${LOCAL_PORT}/tcp; then
  echo "Port $LOCAL_PORT already taken, please make it available or change it in $0"
  exit 1
fi

step "Installing testing tools..."
ssh $SSH_ARGS $awx_host_login "sudo yum -q -y install time tmux"

step "Installing dependencies..."
ssh $SSH_ARGS $awx_host_login "sudo yum -q -y install git make ansible libselinux-python3 docker docker-compose"

step "Installing AWX CLI..."
ssh $SSH_ARGS $awx_host_login "sudo pip3 install awxkit"

step "Disabling SELinux..."
ssh $SSH_ARGS $awx_host_login "sudo setenforce 0"

step "Setting up docker (for user $user)..."
ssh $SSH_ARGS $awx_host_login "sudo groupadd docker"
ssh $SSH_ARGS $awx_host_login "sudo usermod -G docker -a $user"
ssh $SSH_ARGS $awx_host_login "sudo systemctl start docker"

step "Cloning the AWX repository..."
ssh $SSH_ARGS $awx_host_login "git clone https://github.com/ansible/awx.git >/dev/null"
ssh $SSH_ARGS $awx_host_login "cd awx; git checkout 7faf9c62676b0b4189c4401d08ddaac173f21907"

AWX_LOG="/tmp/awx_deploy.log"
step "Deploying AWX. Details are in the $AWX_LOG file ('tail -f $AWX_LOG' to see progress)..."
ssh $SSH_ARGS $awx_host_login "cd awx/installer; ansible-playbook -i inventory install.yml" > "$AWX_LOG"

step "Triggering AWX migrations (should be part of deployment, but it doesn't work there)..."
sleep 1m # Make sure AWX reached the point where it can trigger migrations
ssh $SSH_ARGS $awx_host_login "docker exec awx_task awx-manage migrate --noinput" >> "$AWX_LOG" 2>&1
ssh $SSH_ARGS $awx_host_login "docker restart awx_task"
ssh $SSH_ARGS $awx_host_login "docker restart awx_web"

ssh $SSH_ARGS $awx_host_login -N -L $LOCAL_PORT:localhost:80 >/dev/null &
ssh_tunnel_pid=$!
step "Web console available at http://localhost:$LOCAL_PORT (user: admin, password: password)"
step "(use 'kill $ssh_tunnel_pid' to close the SSH tunnel)"

step "Uploading the SSH key to AWX..."
scp $SSH_ARGS "$key_path" $awx_host_login:
retries=6
while [ $retries -gt 0 ]; do
  if ssh $SSH_ARGS $awx_host_login "awx login --conf.host http://localhost --conf.username admin --conf.password password" >> "$AWX_LOG"; then
    break
  else
    echo "Retrying..."
    retrires=$((retries - 1))
    sleep 10s
  fi
done
if [ $retries -eq 0 ]; then
  echo "Failed to log in to AWX"
  exit 1
fi
ssh $SSH_ARGS $awx_host_login "awx credentials create --conf.host http://localhost \
     --credential_type 'Machine' \
     --name 'Main SSH Key' --user 'admin' \
     --inputs '{\"username\": \"$user\", \"ssh_key_data\": \"@~/$(basename $key_path)\"}'" >> "$AWX_LOG"

step "Success! AWX deployed at $awx_host_login"
